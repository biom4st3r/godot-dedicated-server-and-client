extends Node

var port = 17771

var players = {};

func _enter_tree():
	self.name = '000';

func _ready():
	var network = ENetMultiplayerPeer.new();
	network.create_server(port);
	print('listening: ', port)
	multiplayer.multiplayer_peer = network;
	multiplayer.peer_connected.connect(func(id):
		print('connected: ', id);
		players[id] = null; #Hashset
	)
	multiplayer.peer_disconnected.connect(func(id):
		print('disconnected: ', id);
		if id in players:
			players.erase(id);
	)

@rpc(any_peer, call_remote)
func client_sent_message(msg: String):
	var sender = multiplayer.get_remote_sender_id();
	# Alternativly, rpc_id('server_broadcast_msg', '%s: %s' % [str(sender), msg]); # will send to all connections
	for x in players.keys():
		rpc_id(x, 'server_broadcast_msg', '%s: %s' % [str(sender), msg]);
	print(msg);
	
@rpc
func server_broadcast_msg(msg: String):
	# Dummy
	pass
