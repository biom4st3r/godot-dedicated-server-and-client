class_name Client extends Node

var addr = '10.0.0.70'
var port = 17771
func _enter_tree():
	self.name = '000';

func _ready():
	
	var network = ENetMultiplayerPeer.new();
	network.create_client(addr, port);
	multiplayer.multiplayer_peer = network;
	multiplayer.connected_to_server.connect(func():
		print('connected_to_server')
	)
	multiplayer.connection_failed.connect(func():
		print('connection_failed')
	)
	multiplayer.server_disconnected.connect(func():
		print('server_disconnected')
	)
	# set_multiplayer_authority(multiplayer.get_unique_id())
	# print(is_multiplayer_authority());


func _on_chat_box_message_sent(msg: String):
	rpc("client_sent_message", msg);

@rpc # rpc methods must exist on both client and server	
func client_sent_message(msg: String):
	# Dummy
	pass

@rpc(call_remote)
func server_broadcast_msg(msg: String):
	print('received broadcast');
	var container = $ChatBox/VBoxContainer
	var label = Label.new();
	label.text = msg;
	container.add_child(label);
