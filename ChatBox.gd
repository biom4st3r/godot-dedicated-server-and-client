extends ColorRect

signal message_sent(msg: String)

@onready var text_box = $TextEdit;
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _input(event):
	if Input.is_key_pressed(KEY_ENTER):
		submit();

func _on_button_pressed():
	submit();

func submit():
	if len(text_box.text) == 0:
		return;
	message_sent.emit(text_box.text);
	text_box.text = '';
